var myGamePiece;
var keys = {};
var asteroids = []; 
var gameStartTime;
var gamePlayTime = 0;
var br = 1
var a

//funkcija za prvo pokretanje stranice
function startGame() {
    gameStartTime = Date.now();
    myGamePiece = new component(60, 60, "red", window.innerWidth / 2, window.innerHeight / 2);
    var a1 = [30, 30, "#666565"]
    var a2 = [60, 60, "#827e7e"]
    var a3 = [100, 100, "#949292"]
    var a4 = [130, 130, "#4b505e"]
    a = [a1, a2, a3, a4]
    //stvaranje novih asteroida
    var brTimer = Date.now();
    setInterval(function() {
        if (Date.now() - brTimer > 9800) {
            brTimer = Date.now()
            br += 1
        }
        for (var i = 0; i < br; i++) {
            var asteroid = getRandomAsteroid();
            asteroids.push(asteroid);
        }
    }, 5000);  
    myGameArea.start();

    //listener za tipkovnicu
    window.addEventListener('keydown', function (e) {
        keys[e.code] = true;
    });

    window.addEventListener('keyup', function (e) {
        keys[e.code] = false;
    });
}

//pokretanje igrice i zaustavljanje
var myGameArea = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.id = "myCanvas";
        this.canvas.width = window.innerWidth - 6;
        this.canvas.height = window.innerHeight - 6;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.frameNo = 0;
        this.interval = setInterval(updateGameArea, 20);
    },
    stop : function() {
        clearInterval(this.interval);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height, );
    }
}

//komponent letjelice
function component(width, height, color, x, y, type) {
    this.type = type;
    this.width = width;
    this.height = height;
    this.speed_x = 3.5;
    this.speed_y = 3.5;
    this.x = x;
    this.y = y;
    this.update = function() {
        var ctx = myGameArea.context;
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.fillStyle = color;
        ctx.shadowColor = "red";
        ctx.shadowBlur = 20;
        ctx.fillRect(0, 0, this.width, this.height);
        ctx.restore();
    }
    //pomicanje letjelice strijelicama
    this.newPos = function() {
        if (keys['ArrowLeft'] && this.x - this.width / 2 > 0) {
            this.x -= this.speed_x;
        }
        if (keys['ArrowRight'] && this.x + this.width / 2 < myGameArea.canvas.width) {
            this.x += this.speed_x;
        }
        if (keys['ArrowUp'] && this.y - this.height / 2 > 0) {
            this.y -= this.speed_y;
        }
        if (keys['ArrowDown'] && this.y + this.height / 2 < myGameArea.canvas.height) {
            this.y += this.speed_y;
        }
    }
}

//komponenta asteroida
function componentAsteroid(width, height, color, x, y, type) {
    this.type = type;
    this.width = width;
    this.height = height;
    this.speed_x = 2;
    this.speed_y = 2;
    this.x = x;
    this.y = y;
    this.pocX = x
    this.pocY = y
    this.speed_x = 1 + Math.random() * 4;
    this.speed_y = 0.5 + Math.random() * 3;
    this.update = function() {
        var ctx = myGameArea.context;
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.fillStyle = color;
        ctx.shadowColor = "grey";
        ctx.shadowBlur = 10;
        ctx.fillRect(0, 0, this.width, this.height);
        ctx.restore();
    }
    //rendom smijer asteroida
    this.newPos = function() {
        if (this.pocX - this.width / 2 < myGameArea.context.canvas.width / 2)
            this.x += this.speed_x;
        else 
            this.x -= this.speed_x;
        if (this.pocY - this.height / 2 < myGameArea.context.canvas.height / 2)
            this.y += this.speed_y;
        else 
            this.y -= this.speed_y;
    }
}

// funkcija koja apdejta stranicu svakih 20 milisekuni
function updateGameArea() {
    myGameArea.clear();
    myGamePiece.newPos();

    //nove pozije asteroida
    for (var i = 0; i < asteroids.length; i++) {
        asteroids[i].newPos();
        if (asteroids[i].x < -asteroids[i].width - 200 || asteroids[i].x > myGameArea.canvas.width + 200 || asteroids[i].y < -asteroids[i].height - 200 || asteroids[i].y > myGameArea.canvas.height + 200) {
            asteroids.splice(i, 1);
            i--;
        }
    }

    //provjera kolizije izmedu letjelice i asteroida
    for (var i = 0; i < asteroids.length; i++) {
        if (checkCollision(myGamePiece, asteroids[i])) {
            restartGame();
        }
    }

    //renderanje nove pozicije asteroida
    myGamePiece.update();
    for (var i = 0; i < asteroids.length; i++) {
        asteroids[i].update();
    }

    //vremenski prikaz
    var gameEndTime = Date.now();
    gamePlayTime = gameEndTime - gameStartTime;
    var seconds = ((gamePlayTime / 1000) % 60).toFixed(3);
    var minutes = Math.floor(gamePlayTime / (1000 * 60));
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    myGameArea.context.font = "20px Arial";
    myGameArea.context.fillStyle = "white";
    myGameArea.context.fillText(
        "Gameplay Time: " + minutes + ":" + seconds,
        10,
        30
    );

    var bestTime = localStorage.getItem('bestTime');
    if (bestTime) {
        var bestSeconds = ((parseInt(bestTime) / 1000) % 60).toFixed(3);
        var bestMinutes = Math.floor(parseInt(bestTime) / (1000 * 60));
        bestMinutes = bestMinutes < 10 ? '0' + bestMinutes : bestMinutes;
        bestSeconds = bestSeconds < 10 ? '0' + bestSeconds : bestSeconds;
        myGameArea.context.fillText(
            "Best Time: " + bestMinutes + ":" + bestSeconds,
            10,
            60
        );
    } else {
        myGameArea.context.fillText(
            "Best Time: 00:00.000",
            10,
            60
        );
    }
}

//dobivankje jedne od 4 vrste asteroida
function getRandomAsteroid() {
    var asteroid = a[Math.floor(Math.random() * a.length)];
    var r = Math.random() * 2
    if (r < 1) {
        var r1 = Math.random() * 2
        if (r1 < 1) {
            var x = 0 - asteroid[0]
        } else {
            var x = myGameArea.canvas.width;
        }
        var y = Math.random() * myGameArea.canvas.height;
    } else {
        var r1 = Math.random() * 2
        if (r1 < 1) {
            var y = 0 - asteroid[0]
        } else {
            var y = myGameArea.canvas.height;
        }
        var x = Math.random() * myGameArea.canvas.width;
    }
    return new componentAsteroid(asteroid[0], asteroid[1], asteroid[2], x, y);
}

// provjera kolizije
function checkCollision(obj1, obj2) {
    return (obj1.x <= obj2.x + obj2.width && obj1.x + obj1.width >= obj2.x && obj1.y <= obj2.y + obj2.height && obj1.y + obj1.height >= obj2.y);
}

//restartiranje igrice nakon kolizije
function restartGame() {
    myGameArea.stop();
    var gameEndTime = Date.now();
    gamePlayTime = gameEndTime - gameStartTime;
    var bestTime = localStorage.getItem('bestTime');

    if (!bestTime || gamePlayTime > parseInt(bestTime)) {
        localStorage.setItem('bestTime', gamePlayTime.toString());
    }
    setTimeout(function() {
        asteroids = [];
        myGamePiece.x = window.innerWidth / 2
        myGamePiece.y = window.innerHeight / 2
        gameStartTime = Date.now();
        br = 1
        myGameArea.start();
    }, 10);
}
    
  